#!/usr/bin/env python3

import rospy
from geometry_msgs.msg import Twist
import sys, select, termios, tty
import signal

def getKey():
    tty.setraw(sys.stdin.fileno())
    select.select([sys.stdin], [], [], 0)
    key = sys.stdin.read(1)
    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
    return key

def signal_handler(sig, frame):
    sys.exit(0)

if __name__ == '__main__':
    rospy.init_node('turtle_keyboard')
    pub = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=1)

    rate = rospy.Rate(10) 
    settings = termios.tcgetattr(sys.stdin)

    signal.signal(signal.SIGINT, signal_handler)

    try:
        while not rospy.is_shutdown():
            key = getKey()
            twist_cmd = Twist()

            if key == 'w':
                twist_cmd.linear.x = 1.0
            elif key == 's':
                twist_cmd.linear.x = -1.0
            elif key == 'a':
                twist_cmd.linear.y = 1.0
            elif key == 'd':
                twist_cmd.linear.y = -1.0
            elif key == 'q':
                twist_cmd.angular.z = 1.0
            elif key == 'e':
                twist_cmd.angular.z = -1.0
            elif key == '\x03':  # Strg+C (Ende der Tastatureingabe)
                break

            pub.publish(twist_cmd)
            
            twist_cmd.linear.x = 0.0
            twist_cmd.linear.y = 0.0
            twist_cmd.angular.z = 0.0

            rate.sleep()

    finally:
        termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
        
