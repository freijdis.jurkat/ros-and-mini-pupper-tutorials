#!/usr/bin/env python3

import rospy
from turtlesim.msg import Color

def callback(msg):
    if msg.r == 255 and msg.g == 0 and msg.b == 0:
        print("My trail is red!")
    elif msg.r == 0 and msg.g == 255 and msg.b == 0:
        print("My trail is green!")
    elif msg.r == 0 and msg.g == 0 and msg.b == 255:
        print("My trail is blue!")
    else:
        print(msg)

rospy.init_node("color")
rospy.Subscriber("/turtle1/color_sensor", Color, callback)

rospy.spin()