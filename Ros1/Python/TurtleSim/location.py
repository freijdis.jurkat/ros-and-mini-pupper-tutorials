#!/usr/bin/env python3

import rospy
from turtlesim.msg import Pose

def callback(msg):
    print(msg)
    print()

rospy.init_node("location")
rospy.Subscriber("/turtle1/pose", Pose, callback)

rospy.spin()
