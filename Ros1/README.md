# ROS1 Einsteigercodes

## C++
Der C++-Ordner enthält Code für einen einfachen Publisher und Subscriber, sowie eine angepasste CMakeLists.txt und package.xml.

## Python
Der Python-Ordner enthält Code für einen einfachen Publisher, Subscriber und eine start.launch zum starten der beiden Nodes. Des Weiteren sind Skripte zum Üben mit der TurtleSim enthalten.