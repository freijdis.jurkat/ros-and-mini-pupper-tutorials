# ROS and Mini Pupper Tutorials

## Dokumentation
Enthält:
- Anleitung zur Inbetriebnahme des Puppys Hardware V1 mit ROS1 Noetic
- ROS1 Einsteigerkurs mit Verbindung zum Puppy
- Anleitung zur Erstellung eines Object-Detection-Models mit Tensorflow

## ROS1
Enthält Beispielcode in Python und C++ zur Einführung in das Arbeiten mit ROS1.

## Puppy
Enthält:
- Testprogramme zu Ansteuerung einzelner Baugruppen des Puppys
- object_detection-Package
- Ordner für unterschiedliche Maps, die mit SLAM erstellt wurden
