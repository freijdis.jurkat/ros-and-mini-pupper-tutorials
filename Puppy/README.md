# Mini Pupper

## Ansteuerung der einzelnen Baugruppen
Im Test-Ordner befinden sich Tesprogramme, mit denen man folgende Baugruppen ansteuern kann:
- Audioausgabe
- Batteriemonitor
- Display
- einzelne Servos

## Navigation
Im Maps-Ordner befinden sich verschiedene Maps, die mit dem Mini Pupper erstellt wurden.

## Objekterkennung und -verfolgung
"object_following" ist das Package, das zur Objektverfolgung dient. Voraussetzung ist das [depthai](https://github.com/luxonis/depthai-ros)-Package.
```
echo 'SUBSYSTEM=="usb", ATTRS{idVendor}=="03e7", MODE="0666"' | sudo tee /etc/udev/rules.d/80-movidius.rules
sudo udevadm control --reload-rules && sudo udevadm trigger
sudo apt install ros-<distro>-depthai-ros
```
### Objekterkennung
Starten mit:
```
roslaunch depthai_examples mobile_publisher.launch
rosrun object_following object_visualizer.py
```
Das standartmäßig verwendete Mode ist "mobilenet-ssd".
### Objektverfolgung
Vier von 20 ausgewählten Objekten (definierbar in object_database.py) kann der Mini Pupper erkennen und verfolgen. Mittels PS4 Controller kann man entscheiden, welches der vier Objekte verfolgt werden soll.
Standart ist:
- Viereck: Katze
- Kreuz: Hund
- Kreis: Pferd
- Dreieck: Kuh

Starten mit:
```
roslaunch mini_pupper_bringup bringup.launch lidar_connected:=false
ds4drv
roslaunch object_following detections.launch
roslaunch object_following object_following.launch

## detections.launch:
##rosrun object_following mobilenet_detections.py
##rosrun object_following object_visualizer.py

## object_following.launch:
##rosrun object_following select_object.py
##rosrun object_following filter_detections.py
```

DepthAI-Pipeline:
![](Documentation/DepthAI-Pipeline.png)
