#!/usr/bin/env python3
import rospy
import time
from std_msgs.msg import String
from sensor_msgs.msg import Joy
from MangDang.mini_pupper.display import Display
from object_database import square, cross, circle, triangle

BUTTONS = 14 		#number of buttons in ps4-controller, shouldn't change
#data.buttons = [square, cross, circle, triangle, L1, R1, L2, R2, SHARE, OPTIONS, leftLever, rightLever, home, touchpadButton]

wanted_object = "None"
prev_button_state = [False] * BUTTONS
disp = Display()

def display_image():
	global wanted_object
	path = '/home/ubuntu/catkin_ws/src/object_following/resources/Images/{}.png'.format(wanted_object)
	disp.show_image(path)

def callback(data):
	global BUTTONS
	global wanted_object
	buttons = data.buttons
	
	if buttons[0] and not prev_button_state[0]:
		print("square")
		wanted_object = square
		display_image()		
		
	elif buttons[1] and not prev_button_state[1]:
		print("cross")
		wanted_object = cross
		display_image()		
		
	elif buttons[2] and not prev_button_state[2]:
		print("circle")
		wanted_object = circle
		display_image()	
		
	elif buttons[3] and not prev_button_state[3]:
		print("triangle")
		wanted_object = triangle
		display_image()
		
	elif buttons[9] and not prev_button_state[9]:
		print("IP Adress")
		disp.show_ip()
		
	for i in range(BUTTONS):
		prev_button_state[i] = buttons[i]
		
	pub_wanted_object.publish(wanted_object)
		
def listener():
	rospy.init_node('select_object', anonymous=True)
	rospy.Subscriber("joy", Joy, callback)
	rospy.spin()

if __name__ == '__main__':	
	pub_wanted_object = rospy.Publisher('/wanted_object', String, queue_size=1)
	listener()

		


