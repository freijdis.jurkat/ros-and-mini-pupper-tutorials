#!/usr/bin/env python3

import rospy
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Pose
from std_msgs.msg import String
from object_following.msg import ObjectInformation
from object_database import square, cross, circle, triangle
import math

BUTTONS = 14 
prev_button_state = [False] * BUTTONS

vel = Twist()
pose = Pose()

MAX_X = rospy.get_param('/gait/max_linear_velocity_x')
MAX_Y = rospy.get_param('/gait/max_linear_velocity_y')
MAX_Z = rospy.get_param('/gait/max_angular_velocity_z')
HEIGHT = rospy.get_param('/gait/nominal_height')

#size of the frame in pixels
height = 300
width = 300

#pose based on these variables
roll = 0
roll_max = 0.3
pitch=0
yaw = 0
yaw_increment=0
pitch_increment=0

program_start = False

#information about if wanted object is detected
object_name = "None"
object_center_x = -1
object_center_y = -1
object_distance = -1

wanted_object = "None"
last_seen_loc = -1
last_seen_obj = "None"

status_string = "None"
none_counter = 0

#only prints status if it has changed
def print_status(status):
	global status_string
	if status != status_string:
		status_string = status
		print(status_string)
		
def get_vel_ang_z():
	global last_seen_loc
	if last_seen_loc >= 200:
		vel_ang_z = 0.1 * (-1) * MAX_Z	
	elif last_seen_loc <= 100:
		vel_ang_z = 0.1 * MAX_Z	
	else:
		vel_ang_z = 0
	return vel_ang_z
	
def get_pose(r, p, y):
	global roll, pitch, yaw, pose
	roll = r
	pitch = p
	yaw = y
	
	cy=math.cos(yaw * 0.5)
	sy=math.sin(yaw * 0.5)
	cp=math.cos(pitch * 0.5)
	sp=math.sin(pitch * 0.5)
	cr =math.cos(roll * 0.5)
	sr =math.sin(roll * 0.5)

	pose.orientation.w= cy * cp * cr + sy * sp * sr
	pose.orientation.x = cy * cp * sr - sy * sp * cr
	pose.orientation.y = sy * cp * sr + cy * sp * cr
	pose.orientation.z = sy * cp * cr - cy * sp * sr

def reset_pose():
	global roll, pitch, yaw, pose
	pub_pose = rospy.Publisher('target_body_pose', Pose, queue_size=1)
	roll = 0
	pitch = 0
	yaw = 0
	pose.position.z = 0
	get_pose(roll, pitch, yaw)
	pub_pose.publish(pose)

#try to get object in the middle of the frame via pose	
def toward_obj():
	global pose, roll, pitch, yaw, yaw_increment, pitch_increment, object_center_x, object_center_y
	pub_pose = rospy.Publisher('target_body_pose', Pose, queue_size=1)
	none_counter = 0
	
	while not (130 <= object_center_x <= 170 and 100 <= object_center_y <= 200):		
		
		#buffer is object is in frame but not detected
		if object_center_x == -1 or object_name == "None":
			none_counter += 1
			if none_counter < 40:
				continue
			else:
				break
		if yaw < -0.5 or yaw > 0.5 or pitch < -0.4 or pitch > 0.4:
			print("Yaw or Pitch out of range!")
			reset_pose()
			break
		
		print_status("finetuning")	
		yaw_increment=(width/2-object_center_x)*0.0002
		pitch_increment=-(height/2-object_center_y)*0.0002
		yaw = yaw+yaw_increment
		pitch = pitch+pitch_increment
		get_pose(roll, pitch, yaw)

		pub_pose.publish(pose)
		
		rospy.sleep(0.1)

#tries to find object based on its last known location
def auto_walk():
	global wanted_object, object_name, object_center_x, object_distance, last_seen_loc, none_counter	
	
	pub_vel = rospy.Publisher('cmd_vel', Twist, queue_size=1)
	pub_pose = rospy.Publisher('target_body_pose', Pose, queue_size=1)
	
	if object_center_x != -1:
		last_seen_loc = object_center_x
	
	#prevents from directly restart search for object if object is in frame but not detected
	#restart if object is not detected 40 times
	if none_counter < 40:
		last_seen_obj = object_name
	else:
		last_seen_obj = "None"
	
	#checks if an object to search for is chosen
	if wanted_object != "None":
		
		#checks if there is the wanted object in the frame (last 40 messages)
		if last_seen_obj != "None":

			if object_distance > 400:
				reset_pose()
				vel.linear.x = MAX_X * 0.8
				vel.angular.z = get_vel_ang_z()
				print_status("I'm walking towards the {}.".format(object_name))
			#try to get object in the middle of the frame			
			else: 
				vel.linear.x = 0
				rospy.sleep(1)
				toward_obj()
				#vel.angular.z = get_vel_ang_z() #alternative for toward_obj()
				print_status("I found the {}.".format(object_name))
		
		#if object not in the frame restart search based on the last known location
		else:
			reset_pose()
			if last_seen_loc == -1 or last_seen_loc >= 150: #pixels of the frame = 300
				print_status("I'm searching right.")
				vel.linear.x = 0
				vel.angular.z = (-1) * MAX_Z
				
			else:
				print_status("I'm searching left.")
				vel.linear.x = 0
				vel.angular.z = MAX_Z
				
	else:
		reset_pose()
		vel.linear.x = 0		
		vel.angular.z = 0
		print_status("Please select an Object that i shall detect.")
	
	pub_vel.publish(vel)

#controlling via joystick
def joy_walk(data):	
	global vel, pose, roll
	axes = data.axes
	buttons = data.buttons

	pub_vel = rospy.Publisher('cmd_vel', Twist, queue_size=10)
	pub_pose = rospy.Publisher('target_body_pose', Pose, queue_size=10)
	rate = rospy.Rate(200)

	if not buttons[7]:
		#cmd_vel control
		vel.linear.x = axes[1]*MAX_X
		vel.linear.y = axes[0]*MAX_Y
		vel.angular.z = 2*axes[2]*MAX_Z
		pitch = 0
		yaw = 0

	else:
		#pose(pitch&yaw) control
		vel.linear.x = 0
		vel.linear.y = 0
		vel.angular.z = 0
		pitch = -axes[5]*0.4
		yaw = axes[2]*0.5

	#height control
	if(axes[10]==-1):
		pose.position.z = pose.position.z - 0.0005
	elif(axes[10]==1):
		pose.position.z = pose.position.z + 0.0005
	if(pose.position.z>=0):
		pose.position.z=0
	elif(pose.position.z<=-1*HEIGHT+0.03):
		pose.position.z=-1*HEIGHT+0.03

	#roll control
	if(axes[9]==1):
		roll = roll - 0.005
	elif(axes[9]==-1):
		roll = roll + 0.005
	if(roll>=roll_max):
		roll=roll_max
	elif(roll<=-1*roll_max):
		roll=-1*roll_max

	#clear button
	if buttons[6]:
		roll = 0
		pitch = 0
		yaw = 0
		vel.linear.x = 0
		vel.linear.y = 0
		vel.angular.z = 0
		pose.position.z = 0
		
	get_pose(roll, pitch, yaw)
	pub_vel.publish(vel)
	pub_pose.publish(pose)
	#rospy.loginfo(pose)

def walk_decision(data):
	global program_start
	auto_walk() if program_start else joy_walk(data)

#checks if the pupper shall do autonomous walk to find object or is controllable via joystick	
def start_callback(data):
	global program_start
	buttons = data.buttons
	axes = data.axes
	if buttons[5] and not prev_button_state[5]:
		program_start = not program_start
		if program_start:
			print("Autonomous Walk activated.")
		else:
			print("Control me via PS4-Controller.")
	
	#if u use the levers of the controller to navigate the puppy manually, autonomous walking program will stop	
	if axes[0] != 0 or  axes[1] != 0 or axes[2] != 0 or axes[5] != 0:
		program_start = False
	
	for i in range(BUTTONS):
		prev_button_state[i] = buttons[i]
	
	walk_decision(data)

#contains information about if wanted object is detected and if yes where it is	
def object_callback(msg):
	global object_name, object_center_x, object_center_y, object_distance, none_counter
	object_name = msg.name
	object_center_x = msg.center_x
	object_center_y = msg.center_y
	object_distance = msg.distance
	
	if object_name == "None":
		none_counter += 1
	else:
		none_counter = 0
	
def wanted_callback(msg):
	global wanted_object
	wanted_object = msg.data
	     
def main():
	rospy.init_node('walking', anonymous=True)
	rospy.Subscriber("joy", Joy, start_callback)
	rospy.Subscriber('/object_information', ObjectInformation, object_callback)
	rospy.Subscriber('/wanted_object', String, wanted_callback)
	rospy.spin()
        
if __name__ == '__main__':
	print()
	print("Press R1 to start or stop the Object Following.")
	print("Press Square to detect {}s.".format(square))
	print("Press Cross to detect {}s.".format(cross))
	print("Press Circle to detect {}s.".format(circle))
	print("Press Triangle to detect {}s.".format(triangle))
	print()
	
	main()
