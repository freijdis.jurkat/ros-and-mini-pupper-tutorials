#!/usr/bin/env python3
import rospy
import cv2
from depthai_ros_msgs.msg import SpatialDetectionArray
from sensor_msgs.msg import Image
from object_database import objects
from cv_bridge import CvBridge

cv_image = None

def image_callback(image_msg):
	global cv_image
	cv_image = CvBridge().imgmsg_to_cv2(image_msg, "bgr8")
	
def get_score(score):
	detec_score = int(score * 100)
	approx_score = 100
	while approx_score >= 50:
		if detec_score == approx_score:
			return f"={approx_score}%"
		elif detec_score > approx_score:
			return f">{approx_score}%"
		approx_score -= 10
	return "<50%"

def detection_callback(detections):
	global cv_image
	if cv_image is None:
		return

	# get bounding box measurement and location and object names
	for detection in detections.detections:
		bbox = detection.bbox
		center_x = int(bbox.center.x)
		center_y = int(bbox.center.y)
		size_x = int(bbox.size_x)
		size_y = int(bbox.size_y)
		x1 = center_x - size_x // 2
		y1 = center_y - size_y // 2
		x2 = center_x + size_x // 2
		y2 = center_y + size_y // 2
		object_id = int(detection.results[0].id)
		score = float(detection.results[0].score)
		#object_score = str(round(score * 100, 2)) + "%"
		approx_score = get_score(score)
		object_distance = str(int(detection.position.z)) + "mm"
		
		# convert object_id to object_name
		if object_id in objects:
			object_name, object_color = objects[object_id]
		else:
			object_name = "unknown"
			object_color= (0, 0, 0)
		
		cv2.rectangle(cv_image, (x1, y1), (x2, y2), object_color, 2)
		cv2.putText(cv_image, object_name, (x1 + 5, y1 + 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, object_color, 2)
		#cv2.putText(cv_image, object_score, (x1 + 5, y1 + 30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, object_color, 2)
		cv2.putText(cv_image, approx_score, (x1 + 5, y1 + 30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, object_color, 2)
		cv2.putText(cv_image, object_distance, (x1 + 5, y1 + 45), cv2.FONT_HERSHEY_SIMPLEX, 0.5, object_color, 2)


	# show pictures with bounding boxes and names in image view 
	cv2.imshow("Object Detection", cv_image)
	cv2.waitKey(1)
    
def object_visualizer():
	rospy.init_node('object_visualizer_depth', anonymous=True)
	rospy.Subscriber('/rgb_image', Image, image_callback)
	rospy.Subscriber('/mobilenet_detections', SpatialDetectionArray, detection_callback)
	rospy.spin()

if __name__ == '__main__':
	object_visualizer() 
