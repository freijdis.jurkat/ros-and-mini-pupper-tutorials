#!/usr/bin/env python3

#ObjectID: name, frame_color	
objects = {
	0: ("background", (0, 0, 0)),		
	1: ("aeroplane", (255, 0, 0)),
	2: ("bicycle", (0, 255, 0)),
	3: ("bird", (0, 0, 255)),
	4: ("boat", (255, 255, 0)),
	5: ("bottle", (255, 0, 255)),
	6: ("bus", (0, 255, 255)),
	7: ("car", (128, 128, 0)),
	8: ("cat", (128, 0, 128)),
	9: ("chair", (0, 128, 128)),
	10: ("cow", (0, 0, 128)),
	11: ("diningtable", (128, 128, 128)),
	12: ("dog", (255, 165, 0)),
	13: ("horse", (255, 105, 180)),
	14: ("motorbike", (210, 180, 140)),
	15: ("person", (0, 128, 0)),
	16: ("pottedplant", (184, 134, 11)),
	17: ("sheep", (0, 255, 127)),
	18: ("sofa", (75, 0, 130)),
	19: ("train", (255, 99, 71)),
	20: ("tvmonitor", (240, 128, 128))	
}	

square = "cat"
cross = "dog"
circle = "horse"
triangle = "bottle"
