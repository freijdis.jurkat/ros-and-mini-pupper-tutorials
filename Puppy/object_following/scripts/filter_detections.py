#!/usr/bin/env python3
import rospy
from std_msgs.msg import String
from object_following.msg import ObjectInformation
from depthai_ros_msgs.msg import SpatialDetectionArray
from object_database import objects

wanted_object = "None"
object_name = "None"
center_x = -1 
center_y = -1
distance = -1

def reset_object_info():
	global object_name, center_x, center_y, distance
	object_name = "None"
	center_x = -1
	center_y = -1
	distance = -1

def pub_object_info():
	global object_name, center_x, center_y, distance
	msg = ObjectInformation()
	msg.name = object_name
	msg.center_x = center_x 
	msg.center_y = center_y
	msg.distance = distance
	pub_object_information.publish(msg)

def wanted_callback(msg):
	global wanted_object
	wanted_object = msg.data	

def detected_object_callback(detections):
	global wanted_object, object_name, center_x, center_y, distance
	
	if not detections.detections:
		reset_object_info()
	
	else:	
		for detection in detections.detections:
	
			object_id = int(detection.results[0].id)
		
			if object_id in objects:
				object_name, _ = objects[object_id] 
			else:
				raise Exception("Object-ID not in Database. Check if you use correct Database for your used Model.")
				break
		
			if object_name == wanted_object:
				center_x = int(detection.bbox.center.x)
				center_y = int(detection.bbox.center.y)
				distance = detection.position.z
				break
			else:
				reset_object_info()
			
	pub_object_info()			

def filter_detections():
	rospy.init_node('filter_detections', anonymous=True)
	rospy.Subscriber('/wanted_object', String, wanted_callback)
	rospy.Subscriber('/mobilenet_detections', SpatialDetectionArray, detected_object_callback)
	rospy.spin()

if __name__ == '__main__':	
	pub_object_information = rospy.Publisher('/object_information', ObjectInformation, queue_size=1)
	filter_detections() 
