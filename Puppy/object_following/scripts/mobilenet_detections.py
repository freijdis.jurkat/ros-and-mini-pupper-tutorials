#!/usr/bin/env python3

import rospy
import cv2
import depthai as dai
import numpy as np
import time
import datetime
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
from depthai_ros_msgs.msg import SpatialDetectionArray, SpatialDetection
from vision_msgs.msg import ObjectHypothesis
from sensor_msgs.msg import Image

def main():
	nnBlobPath = '/home/ubuntu/catkin_ws/src/object_following/resources/Models/mobilenet-ssd_openvino_2021.2_6shave.blob'
	pipeline = dai.Pipeline()	#Pipeline für das DepthAI-Gerät erstellen
	bridge = CvBridge()		#CvBridge für Bildkonvertierung initialisieren
	rate = rospy.Rate(40)		#Rate für ROS-Veröffentlichung festlegen
	
	#Knoten für das MobileNet SpatialDetectionNetwork erstellen
	spatialDetectionNetwork = pipeline.create(dai.node.MobileNetSpatialDetectionNetwork)
	xoutNN = pipeline.create(dai.node.XLinkOut)
	xoutNN.setStreamName("detections")

	#Knoten für die Farbkamera erstellen
	camRgb = pipeline.create(dai.node.ColorCamera)
	xoutRgb = pipeline.create(dai.node.XLinkOut)
	xoutRgb.setStreamName("rgb")
	
	#Knoten für Monokameras und Stereo-Tiefe erstellen
	monoLeft = pipeline.create(dai.node.MonoCamera)
	monoRight = pipeline.create(dai.node.MonoCamera)
	stereo = pipeline.create(dai.node.StereoDepth)
	xoutDepth = pipeline.create(dai.node.XLinkOut)
	xoutDepth.setStreamName("depth")
	
	#Konfigurationen für die Farbkamera
	camRgb.setPreviewSize(300, 300) #Eingabegröße für das NN 
	camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
	camRgb.setInterleaved(False)
	camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)

	#Konfigurationen für Monokameras
	monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
	monoLeft.setCamera("left")
	monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
	monoRight.setCamera("right")
	
	#Konfigurationen für Stereo-Tiefe
	stereo.setDefaultProfilePreset(dai.node.StereoDepth.PresetMode.HIGH_DENSITY)
	stereo.setDepthAlign(dai.CameraBoardSocket.CAM_A)
	stereo.setSubpixel(True)
	stereo.setOutputSize(monoLeft.getResolutionWidth(), monoLeft.getResolutionHeight())
	
	#Konfigurationen für das SpatialDetectionNetwork
	spatialDetectionNetwork.setBlobPath(nnBlobPath)
	spatialDetectionNetwork.setConfidenceThreshold(0.6)
	spatialDetectionNetwork.setBoundingBoxScaleFactor(0.5)
	spatialDetectionNetwork.setDepthLowerThreshold(100)
	spatialDetectionNetwork.setDepthUpperThreshold(5000)	
	spatialDetectionNetwork.input.setBlocking(False)

	#Verbindungen zwischen den Knoten herstellen
	monoLeft.out.link(stereo.left)
	monoRight.out.link(stereo.right)
	camRgb.preview.link(spatialDetectionNetwork.input)
	spatialDetectionNetwork.passthrough.link(xoutRgb.input)
	spatialDetectionNetwork.out.link(xoutNN.input)
	stereo.depth.link(spatialDetectionNetwork.inputDepth)
	spatialDetectionNetwork.passthroughDepth.link(xoutDepth.input)
	
	#DepthAI-Gerät initialisieren
	with dai.Device(pipeline) as device:
		rgbQueue = device.getOutputQueue(name="rgb", maxSize=4, blocking=False)
		depthQueue = device.getOutputQueue(name="depth", maxSize=4, blocking=False)
		detectionNNQueue = device.getOutputQueue(name="detections", maxSize=4, blocking=False)
		
		while not rospy.is_shutdown():
			
			#Daten aus Ausgabeschlangen abrufen
			inRGB = rgbQueue.get()	
			inDepth = depthQueue.get()
			inDet = detectionNNQueue.get()
			
			#Bilder in das OpenCV-Format konvertieren
			rgb_frame = inRGB.getCvFrame()
			depth_frame = inDepth.getFrame()
			detections = inDet.detections
			
			#Abmessungen der Bilder abrufen
			height = rgb_frame.shape[0]
			width  = rgb_frame.shape[1]
			
			#Tiefenbild verkleinern und normalisieren
			depth_downscaled = depth_frame[::4]
			min_depth = np.percentile(depth_downscaled[depth_downscaled != 0], 1)
			max_depth = np.percentile(depth_downscaled, 99)
			depthFrameColor = np.interp(depth_frame, (min_depth, max_depth), (0, 255)).astype(np.uint8)
			depthFrameColor = cv2.applyColorMap(depthFrameColor, cv2.COLORMAP_HOT)
			
			#Bilder in ROS-Nachrichten konvertieren und veröffentlichen
			rgb_msg = bridge.cv2_to_imgmsg(rgb_frame, "bgr8")
			depth_msg = bridge.cv2_to_imgmsg(depthFrameColor, "bgr8") 
			pub_rgb_image.publish(rgb_msg)
			pub_depth_image.publish(depth_msg)
			
			#SpatialDetectionArray erstellen
			detectionArray = SpatialDetectionArray()
			#Auskommentiert, um Rechenleistung zu sparen	
			#detectionArray.header.stamp.secs = int(time.time())
			#detectionArray.header.stamp.nsecs = int((datetime.datetime.now().timestamp() % 1) * 1e9)
			#detectionArray.header.frame_id = "camera_link"
			
			#jedes erkannte Objekt verarbeiten
			for detection in detections:
				spatialDetection = SpatialDetection()
				objectHypothesis = ObjectHypothesis()
			
				#Objekt-ID und Wahrscheinlichkeit				
				objectHypothesis.id = detection.label
				objectHypothesis.score = detection.confidence
				spatialDetection.results.append(objectHypothesis)
				#Objektabstand zur Kameramitte
				spatialDetection.position.x = detection.spatialCoordinates.x
				spatialDetection.position.y = detection.spatialCoordinates.y
				spatialDetection.position.z = detection.spatialCoordinates.z
				#BoundingBox des Objekts
				xmin = int(detection.xmin * width)
				xmax = int(detection.xmax * width)
				ymin = int(detection.ymin * height)
				ymax = int(detection.ymax * height)				
				spatialDetection.bbox.center.x = xmin + (xmax - xmin) // 2
				spatialDetection.bbox.center.y = ymin + (ymax - ymin) // 2
				spatialDetection.bbox.size_x = xmax - xmin
				spatialDetection.bbox.size_y = ymax - ymin
				
				# SpatialDetection zur DetectionArray-Nachricht hinzufügen
				detectionArray.detections.append(spatialDetection)
			
			#DetectionArray-Nachricht veröffentlichen
			pub_detections.publish(detectionArray)
			rate.sleep()

#ROS-Knoten initialisieren
if __name__ == '__main__':
	rospy.init_node("object_detection", anonymous=True)
	pub_detections = rospy.Publisher("mobilenet_detections", SpatialDetectionArray, queue_size=1)
	pub_rgb_image = rospy.Publisher("rgb_image", Image, queue_size=4)
	pub_depth_image = rospy.Publisher("depth_image", Image, queue_size=4)
	
	main()
	

