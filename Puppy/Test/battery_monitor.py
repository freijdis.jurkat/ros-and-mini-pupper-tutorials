#!/usr/bin/env python3

def get_battery_voltage():
    file_path = '/sys/class/power_supply/max1720x_battery/voltage_now'

    try:
        with open(file_path, 'r') as file:
            voltage = int(file.read().strip())

        return voltage
    except FileNotFoundError:
        print("Datei nicht gefunden: Überprüfen Sie den Pfad zur Batterieinformation.")
        return None
    except Exception as e:
        print("Fehler beim Lesen der Batterieinformation:", e)
        return None

voltage = get_battery_voltage()

if voltage is not None:
    print("Der aktuelle Batteriewert beträgt:", voltage, "mV")
else:
    print("Batteriewert konnte nicht abgerufen werden.")

