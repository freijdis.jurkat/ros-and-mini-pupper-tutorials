#!/usr/bin/env python3
from MangDang.mini_pupper.display import Display
import time

disp = Display()

def main():
	while True:
		disp.show_image('/var/lib/mini_pupper_bsp/test.png')
		time.sleep(5)
		disp.show_ip()
		time.sleep(5)

main()
