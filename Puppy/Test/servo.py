#!/usr/bin/env python3

import subprocess
import time

def set_servo_position(duty_cycle):
    pwm_path = '/sys/class/pwm/pwmchip0/pwm4/duty_cycle'
    try:
        with open(pwm_path, 'w') as pwm_file:
            pwm_file.write(str(duty_cycle))
    except IOError as e:
        print("Fehler beim Schreiben des PWM-Werts:", e)

def reset_servo():
    subprocess.run(['echo', '0', '>', '/sys/class/pwm/pwmchip0/pwm4/enable'], check=True)
    subprocess.run(['echo', '0', '>', '/sys/class/gpio/gpio21/value'], check=True)
    subprocess.run(['echo', '0', '>', '/sys/class/gpio/gpio25/value'], check=True)
    time.sleep(1)
    subprocess.run(['echo', '1', '>', '/sys/class/pwm/pwmchip0/pwm4/enable'], check=True)
    subprocess.run(['echo', '1', '>', '/sys/class/gpio/gpio21/value'], check=True)
    subprocess.run(['echo', '1', '>', '/sys/class/gpio/gpio25/value'], check=True)

def main():
    set_servo_position(1500000)
    time.sleep(2)
    set_servo_position(1000000)
    time.sleep(2)
    set_servo_position(1500000)

    reset_servo()

if __name__ == "__main__":
    main()

