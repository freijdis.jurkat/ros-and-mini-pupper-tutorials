#!/usr/bin/env python3

import subprocess

def play_audio(file_path):
    try:
        subprocess.run(['mpg123', '-a', 'hw:01,00', file_path], check=True)
    except subprocess.CalledProcessError as e:
        print("Fehler beim Abspielen des Audios:", e)

audio_file_path = '/home/ubuntu/mini_pupper_bsp/Audio/power_on.mp3'

play_audio(audio_file_path)
